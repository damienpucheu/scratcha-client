/* eslint-disable import/prefer-default-export */

import { call, put } from 'redux-saga/effects';
import { callOAuthProviderFailed } from '../actions';
import openOAuthLoginPage from '../utils/openWindow';

export function* callOAuthProvider(action) {
  try {
    const { provider } = action;
    const url = provider.authEndpoint;
    if (!provider.name || !provider.authEndpoint || !provider.profileEndpoint) {
      throw new Error(`Provider : ${provider.name || null} not recognized`);
    }
    yield call(openOAuthLoginPage, url);
  } catch (error) {
    yield put(callOAuthProviderFailed(error));
  }
}
