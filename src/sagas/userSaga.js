/* eslint-disable import/prefer-default-export */

import { call, put, select } from 'redux-saga/effects';
import { saveTokenInStore, fetchUser, fetchUserSuccess, fetchUserFailed } from '../actions';
import { GET } from '../constants';
import callApi from '../utils/callApi';
import { getToken, getProvider } from '../selectors';

export function* saveToken(action) {
  const { accessToken } = action;
  yield put(saveTokenInStore(accessToken));
  yield put(fetchUser());
}

export function* fetchUserSaga() {
  try {
    const accessToken = yield select(getToken);
    const provider = yield select(getProvider);
    const { data: user } = yield call(
      callApi,
      `${provider.profileEndpoint}?access_token=${accessToken}`,
      GET
    );
    yield put(fetchUserSuccess(user));
  } catch (error) {
    yield put(fetchUserFailed(error));
  }
}
