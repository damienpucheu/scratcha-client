import { all, takeEvery } from 'redux-saga/effects';
import { CALL_OAUTH_PROVIDER, CALL_OAUTH_PROVIDER_SUCCESS, FETCH_USER } from '../constants';
import { callOAuthProvider } from './providerSaga';
import { saveToken, fetchUserSaga } from './userSaga';

export default function* rootSaga() {
  yield all([
    yield takeEvery(CALL_OAUTH_PROVIDER, callOAuthProvider),
    yield takeEvery(CALL_OAUTH_PROVIDER_SUCCESS, saveToken),
    yield takeEvery(FETCH_USER, fetchUserSaga)
  ]);
}
