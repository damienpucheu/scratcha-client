import { SELECT_COUNTRY } from '../constants';

const initialState = {};

const map = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_COUNTRY:
      return { ...state, country: action.country, openSidebar: true };
    default:
      return state;
  }
};

export default map;
