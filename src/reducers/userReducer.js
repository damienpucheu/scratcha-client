import {
  FETCH_USER,
  FETCH_USER_FAILED,
  FETCH_USER_SUCCESS,
  SAVE_TOKEN_IN_STORE
} from '../constants';

const initialState = { loading: false, accessToken: undefined };

const user = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER:
      return { ...state, loading: true };
    case FETCH_USER_SUCCESS:
      return { ...state, loading: false, ...action.user };
    case FETCH_USER_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case SAVE_TOKEN_IN_STORE:
      return {
        ...state,
        accessToken: action.accessToken
      };
    default:
      return state;
  }
};

export default user;
