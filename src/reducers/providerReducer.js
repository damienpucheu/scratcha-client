import { CALL_OAUTH_PROVIDER, CALL_OAUTH_PROVIDER_SUCCESS } from '../constants';

const initialState = {};

const provider = (state = initialState, action) => {
  switch (action.type) {
    case CALL_OAUTH_PROVIDER:
      return { ...state, ...action.provider };
    case CALL_OAUTH_PROVIDER_SUCCESS:
      return { ...state, ...action.provider };
    default:
      return state;
  }
};

export default provider;
