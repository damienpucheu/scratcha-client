import { combineReducers } from 'redux';
import userReducer from './userReducer';
import providerReducer from './providerReducer';
import mapReducer from './mapReducer';

export default combineReducers({
  user: userReducer,
  provider: providerReducer,
  map: mapReducer
});
