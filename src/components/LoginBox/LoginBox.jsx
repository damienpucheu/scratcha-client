import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { providers } from '../../constants';
import styles from './LoginBox.module.scss';

const LoginBox = ({ loading, callProvider }) => (
  <div className={styles.container}>
    {loading ? (
      <FontAwesomeIcon icon={['far', 'compass']} size="3x" spin />
    ) : (
      <>
        <h3>Rassemblez vos souvenirs via :</h3>
        <div className={styles.buttons}>
          <button
            className={styles.igButton}
            type="button"
            id="ig_btn"
            onClick={() => callProvider(providers.instagram)}
          >
            <FontAwesomeIcon className={styles.igIcon} icon={['fab', 'instagram']} size="3x" />
            <h2>Instagram</h2>
          </button>
        </div>
      </>
    )}
  </div>
);

LoginBox.propTypes = {
  loading: PropTypes.bool.isRequired,
  callProvider: PropTypes.func.isRequired
};

export default LoginBox;
