import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { Redirect } from 'react-router-dom';

const Callback = ({ location, match, OAuthSuccess, OAuthFailed }) => {
  const { provider } = match.params;
  const { access_token: accessToken } = queryString.parse(location.hash);
  const { error } = queryString.parse(location.search);
  if (error) OAuthFailed(error);
  if (accessToken && provider) OAuthSuccess(accessToken, provider);
  return <Redirect to="/" />;
};

Callback.propTypes = {
  location: PropTypes.objectOf(PropTypes.string).isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  OAuthSuccess: PropTypes.func.isRequired,
  OAuthFailed: PropTypes.func.isRequired
};

export default Callback;
