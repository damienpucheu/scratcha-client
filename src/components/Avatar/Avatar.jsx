import React from 'react';
import PropTypes from 'prop-types';
import styles from './Avatar.module.scss';

const Avatar = ({ picture, username }) => (
  <div className={styles.container}>
    <img className={styles.picture} src={picture} alt={`profile_${username}`} />
    <h5>{username}</h5>
  </div>
);

Avatar.propTypes = {
  picture: PropTypes.string,
  username: PropTypes.string
};

Avatar.defaultProps = {
  picture: null,
  username: null
};

export default Avatar;
