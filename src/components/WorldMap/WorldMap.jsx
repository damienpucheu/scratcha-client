import React from 'react';
import PropTypes from 'prop-types';

import { ComposableMap, ZoomableGroup, Geographies, Geography } from 'react-simple-maps';
import { scaleLinear } from 'd3-scale';

import topoWorldMap from '../../data/world-110m.json';

import styles from './WorldMap.module.scss';

const colorScale = scaleLinear()
  .domain([0, 200])
  .range(['#ECEFF1', '#607D8B']);

const Worldmap = ({ country, selectCountry }) => {
  return (
    <div className={styles.container}>
      <ComposableMap width={1000} height={600} className={styles.worldmap}>
        <ZoomableGroup disablePanning>
          <Geographies geography={topoWorldMap}>
            {({ geographies }, projection) =>
              geographies.map((geography, index) => (
                <Geography
                  onClick={() => selectCountry(geography.properties)}
                  key={geography.rsmKey}
                  geography={geography}
                  projection={projection}
                  style={{
                    default: {
                      fill:
                        country.NAME === geography.properties.NAME ? '3354ff' : colorScale(index),
                      stroke: '#607D8B',
                      strokeWidth: 0.75,
                      outline: 'none'
                    },
                    hover: {
                      fill: '#3354ff',
                      stroke: '#607D8B',
                      strokeWidth: 0.75,
                      outline: 'none'
                    },
                    pressed: {
                      fill: '#3354ff',
                      stroke: '#607D8B',
                      strokeWidth: 0.75,
                      outline: 'none'
                    }
                  }}
                />
              ))
            }
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
    </div>
  );
};

Worldmap.defaultProps = {
  country: {}
};

Worldmap.propTypes = {
  country: PropTypes.objectOf(PropTypes.any),
  selectCountry: PropTypes.func.isRequired
};

export default Worldmap;
