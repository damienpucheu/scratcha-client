import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import styles from './Sidebar.module.scss';

const Sidebar = ({ open }) => (
  <div className={cn(styles.sidebar, open ? styles.open : styles.close)}>
    <p>About</p>
    <p>Services</p>
    <p>Clients</p>
    <p>Contact</p>
  </div>
);

Sidebar.defaultProps = {
  open: true
};

Sidebar.propTypes = {
  open: PropTypes.bool
};

export default Sidebar;
