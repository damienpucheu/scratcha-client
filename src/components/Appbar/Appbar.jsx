import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './Appbar.module.scss';

import Avatar from '../Avatar';
import Logo from '../Logo';

const Appbar = ({ picture, username }) => (
  <div className={styles.container}>
    <Link to="/">
      <Logo />
    </Link>
    {username ? <Avatar picture={picture} username={username} /> : null}
  </div>
);

Appbar.propTypes = {
  picture: PropTypes.string,
  username: PropTypes.string
};

Appbar.defaultProps = {
  picture: null,
  username: null
};

export default Appbar;
