import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HomePageContainer from '../../containers/HomePageContainer';
import Studio from '../../screens/Studio';
import CallbackContainer from '../../containers/CallbackContainer';

const App = () => (
  <Router>
    <Route path="/" exact render={() => <HomePageContainer />} />
    <Route path="/studio" exact render={() => <Studio />} />
    <Route path="/auth/:provider" render={() => <CallbackContainer />} />
  </Router>
);

export default App;
