import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

import styles from './Button.module.scss';

const Button = ({ text, displayArrow, classname }) => (
  <button className={cn(classname, styles.button)} type="button">
    <div className={styles.buttonContent}>
      <div>{text}</div>
      {displayArrow && <FontAwesomeIcon icon={faArrowRight} className={styles.arrow} />}
    </div>
  </button>
);

Button.propTypes = {
  text: PropTypes.string.isRequired,
  displayArrow: PropTypes.bool,
  classname: PropTypes.string
};

Button.defaultProps = {
  displayArrow: false,
  classname: ''
};

export default Button;
