import { connect } from 'react-redux';
import Sidebar from '../components/Sidebar';

const mapStateToProps = state => ({
  open: state.map.openSidebar
});

const SidebarContainer = connect(mapStateToProps, null)(Sidebar);

export default SidebarContainer;
