import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { callOAuthProviderSuccess, callOAuthProviderFailed } from '../actions';
import Callback from '../components/Callback';

const mapDispatchToProps = dispatch => ({
  OAuthSuccess: (accessToken, provider) => {
    dispatch(callOAuthProviderSuccess(accessToken, provider));
  },
  OAuthFailed: error => {
    dispatch(callOAuthProviderFailed(error));
  }
});

const CallbackContainer = withRouter(connect(null, mapDispatchToProps)(Callback));

export default CallbackContainer;
