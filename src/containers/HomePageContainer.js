import { connect } from 'react-redux';
import { callOAuthProvider } from '../actions';
import HomePage from '../screens/HomePage';

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  callProvider: provider => dispatch(callOAuthProvider(provider))
});

const HomePageContainer = connect(mapStateToProps, mapDispatchToProps)(HomePage);

export default HomePageContainer;
