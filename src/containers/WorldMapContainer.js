import { connect } from 'react-redux';
import { selectCountry } from '../actions';
import WorldMap from '../components/WorldMap';

const mapStateToProps = state => ({
  country: state.map.country
});

const mapDispatchToProps = dispatch => ({
  selectCountry: country => dispatch(selectCountry(country))
});

const WorldMapContainer = connect(mapStateToProps, mapDispatchToProps)(WorldMap);

export default WorldMapContainer;
