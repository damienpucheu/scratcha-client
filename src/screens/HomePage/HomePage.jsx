import React from 'react';
import { Link } from 'react-router-dom';

import Button from '../../components/Button';
import backgroundImage from '../../assets/img/bali.png';

import styles from './HomePage.module.scss';

const HomePage = () => (
  <div className={styles.container}>
    <div className={styles.content}>
      <h1 className={styles.title}>Scratcha.</h1>
      <h3 className={styles.subtitle}>Ravivez vos souvenirs en créant votre carte de voyage</h3>
      <Link to="/studio">
        <Button text="Créer ma carte" displayArrow classname={styles.button} />
      </Link>
    </div>
    <img src={backgroundImage} alt="bali" className={styles.background} />
  </div>
);

export default HomePage;
