import React from 'react';
import Appbar from '../../components/Appbar';
import WorldMap from '../../containers/WorldMapContainer';
import Sidebar from '../../containers/SidebarContainer';

import styles from './Studio.module.scss';

const Studio = () => {
  return (
    <div className={styles.container}>
      <Appbar />
      <div className={styles.content}>
        <WorldMap />
        <Sidebar />
      </div>
    </div>
  );
};

export default Studio;
