import SidebarContainer from '../../containers/SidebarContainer';

describe('SidebarContainer', () => {
  beforeAll(() => {
    jest.restoreAllMocks();
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  it('should set only map from state to props', () => {
    const state = { map: { openSidebar: 'fake-open' } };
    const props = SidebarContainer.mapStateToProps(state);
    expect(props).toEqual({ open: state.map.openSidebar });
  });
});
