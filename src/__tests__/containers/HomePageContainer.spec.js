import HomePageContainer from '../../containers/HomePageContainer';
import { callOAuthProvider } from '../../actions';

describe('HomePageContainer', () => {
  beforeAll(() => {
    jest.restoreAllMocks();
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  it('should set only user from state to props', () => {
    const state = { user: 'fake-user', other: 'fake-other' };
    const props = HomePageContainer.mapStateToProps(state);
    expect(props).toEqual({ user: state.user });
  });
  it('should call oauth provider with provider', () => {
    const fakeProvider = 'fake-provider';
    HomePageContainer.mapDispatchToProps().callProvider(fakeProvider);
    expect(HomePageContainer.mockDispatch).toHaveBeenCalledWith(callOAuthProvider(fakeProvider));
  });
});
