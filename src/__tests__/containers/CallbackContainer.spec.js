import CallbackContainer from '../../containers/CallbackContainer';
import { callOAuthProviderSuccess, callOAuthProviderFailed } from '../../actions';

describe('CallbackContainer', () => {
  beforeAll(() => {
    jest.restoreAllMocks();
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });
  it('should call oauth provider success with access token', () => {
    const fakeAccessToken = 'fake-access-token';
    CallbackContainer.mapDispatchToProps().OAuthSuccess(fakeAccessToken);
    expect(CallbackContainer.mockDispatch).toHaveBeenCalledWith(callOAuthProviderSuccess(fakeAccessToken));
  });
  it('should call oauth provider failed with error', () => {
    const fakeError = 'fake-error';
    CallbackContainer.mapDispatchToProps().OAuthFailed(fakeError);
    expect(CallbackContainer.mockDispatch).toHaveBeenCalledWith(callOAuthProviderFailed(fakeError));
  });
});
