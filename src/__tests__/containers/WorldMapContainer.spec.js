import WorldMapContainer from '../../containers/WorldMapContainer';
import { selectCountry } from '../../actions';

describe('WorldMapContainer', () => {
  beforeAll(() => {
    jest.restoreAllMocks();
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  it('should set only map from state to props', () => {
    const state = { map: { country: 'fake-map' } };
    const props = WorldMapContainer.mapStateToProps(state);
    expect(props).toEqual({ country: state.map.country });
  });

  it('should call select country with country', () => {
    const fakeCountry = 'fake-country';
    WorldMapContainer.mapDispatchToProps().selectCountry(fakeCountry);
    expect(WorldMapContainer.mockDispatch).toHaveBeenCalledWith(selectCountry(fakeCountry));
  });
});
