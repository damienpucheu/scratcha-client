import { callOAuthProvider } from '../../sagas/providerSaga';
import openOAuthLoginPage from '../../utils/openWindow';
import * as actions from '../../actions/providerActions';
import recordSaga from '../../utils/recordSaga';
import { providers } from '../../constants';

jest.mock('../../utils/openWindow');

describe('callOAuthProvider', () => {
  it('should call oauth login page when provider is valid like instagram', async () => {
    const action = { provider: providers.instagram };
    await recordSaga(
      callOAuthProvider,
      action,
    );
    expect(openOAuthLoginPage).toHaveBeenCalledWith(providers.instagram.authEndpoint);
  });
  it('should put oauth provider failed when provider is incorrect', async () => {
    const mockCallFailed = jest.spyOn(actions, 'callOAuthProviderFailed');
    const action = { provider: 'GNUGNUGNU' };
    await recordSaga(
      callOAuthProvider,
      action,
    );
    expect(mockCallFailed).toHaveBeenCalled();
    mockCallFailed.mockRestore();
  });
});
