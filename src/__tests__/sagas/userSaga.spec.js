import { saveToken, fetchUserSaga } from '../../sagas/userSaga';
import * as actions from '../../actions/userActions';
import * as selectors from '../../selectors';
import recordSaga from '../../utils/recordSaga';
import callApi from '../../utils/callApi';

jest.mock('../../utils/callApi');

describe('saveToken', () => {
  it('should call saveToken and fetchUser actions', async () => {
    const mockSaveToken = jest.spyOn(actions, 'saveTokenInStore');
    const mockFetchUser = jest.spyOn(actions, 'fetchUser');
    const action = { accessToken: 'fake-token' };
    await recordSaga(
      saveToken,
      action,
    );
    expect(mockSaveToken).toHaveBeenCalledWith(action.accessToken);
    expect(mockFetchUser).toHaveBeenCalled();
  });
});

describe('fetchUser', () => {
  it('should call fetch user success action if all good', async () => {
    callApi.mockResolvedValue({ data: 'fake-user' });
    const mockFetchUserSuccess = jest.spyOn(actions, 'fetchUserSuccess');
    jest.spyOn(selectors, 'getToken').mockReturnValue('fake-token');
    jest.spyOn(selectors, 'getProvider').mockReturnValue('fake-provider');
    await recordSaga(
      fetchUserSaga,
    );
    expect(mockFetchUserSuccess).toHaveBeenCalledWith('fake-user');
  });
  it('should call fetch user failed action if call failed', async () => {
    callApi.mockRejectedValue('fake-error');
    const mockFetchUserFailed = jest.spyOn(actions, 'fetchUserFailed');
    jest.spyOn(selectors, 'getToken').mockReturnValue('fake-token');
    jest.spyOn(selectors, 'getProvider').mockReturnValue('fake-provider');
    await recordSaga(
      fetchUserSaga,
    );
    expect(mockFetchUserFailed).toHaveBeenCalledWith('fake-error');
  });
});
