import reducer from '../../reducers/mapReducer';
import * as types from '../../constants';

describe('map reducer', () => {
  const initialState = {};

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle SELECT_COUNTRY', () => {
    const country = 'fake-country';
    expect(
      reducer(initialState, {
        type: types.SELECT_COUNTRY,
        country
      })
    ).toEqual({
      ...initialState,
      country,
      openSidebar: true
    });
  });
});
