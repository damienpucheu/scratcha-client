import reducer from '../../reducers/userReducer';
import * as types from '../../constants';

describe('user reducer', () => {
  const initialState = { loading: false, accessToken: undefined };

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle FETCH_USER', () => {
    expect(
      reducer(initialState, {
        type: types.FETCH_USER,
      }),
    ).toEqual(
      {
        ...initialState,
        loading: true,
      },
    );
  });

  it('should handle FETCH_USER_SUCCESS', () => {
    const user = { name: 'fake-user' };
    expect(
      reducer(initialState, {
        type: types.FETCH_USER_SUCCESS,
        user,
      }),
    ).toEqual(
      {
        loading: false,
        ...user,
      },
    );
  });

  it('should handle SAVE_TOKEN_IN_STORE', () => {
    const accessToken = 'fake-token';
    expect(
      reducer(initialState, {
        type: types.SAVE_TOKEN_IN_STORE,
        accessToken,
      }),
    ).toEqual(
      {
        ...initialState,
        accessToken,
      },
    );
  });
});
