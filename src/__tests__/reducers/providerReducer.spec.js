import reducer from '../../reducers/providerReducer';
import * as types from '../../constants';

describe('provider reducer', () => {
  const initialState = {};

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle CALL_OAUTH_PROVIDER', () => {
    const provider = { name: 'fake-provider', authEndpoint: 'fake-auth-endpoint', profileEndpoint: 'fake-profile-endpoint' };
    expect(
      reducer(initialState, {
        type: types.CALL_OAUTH_PROVIDER,
        provider,
      }),
    ).toEqual(
      {
        ...initialState,
        ...provider,
      },
    );
  });
});
