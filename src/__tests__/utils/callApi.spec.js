import callApi from '../../utils/callApi';
import { GET } from '../../constants';

beforeEach(() => {
  jest.clearAllMocks();
});


describe('callApi', () => {
  it('should return response when all good', async () => {
    const fakeResponse = { response: 'fake' };
    const mockFetchPromise = Promise.resolve({
      json: () => Promise.resolve(fakeResponse),
    });
    const mockCall = jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);
    const url = 'fake-url';
    const response = await callApi(url, GET);
    expect(response).toEqual(fakeResponse);
    expect(mockCall.mock.calls[0][0]).toEqual(url);
    expect(mockCall.mock.calls[0][1]).toMatchObject({ method: GET });
  });
});
