import React from 'react';
import { shallow } from 'enzyme';
import Logo from '../../components/Logo';

describe('logo', () => {
  const props = {};
  it('renders without crashing', () => {
    const logo = shallow(<Logo {...props} />);
    expect(logo.length).toEqual(1);
  });

  it('should render correctly with empty props', () => {
    const logo = shallow(<Logo {...props} />);
    expect(logo).toMatchSnapshot();
  });
});
