import React from 'react';
import { shallow } from 'enzyme';
import { Redirect } from 'react-router-dom';
import Callback from '../../components/Callback';

describe('Callback', () => {
  const initialProps = {
    location: { hash: '', search: '' },
    match: { params: '' },
    OAuthSuccess: jest.fn(),
    OAuthFailed: jest.fn()
  };
  it('renders without crashing', () => {
    const callback = shallow(<Callback {...initialProps} />);
    expect(callback.length).toEqual(1);
  });

  it('should render correctly', () => {
    const callback = shallow(<Callback {...initialProps} />);
    expect(callback).toMatchSnapshot();
  });

  it('should return redirect', () => {
    const callback = shallow(<Callback {...initialProps} />);
    expect(callback.find(Redirect)).toHaveLength(1);
  });

  it('should call oauth failed if error detected and return redirect', () => {
    const props = {
      location: { hash: '', search: 'error=fake-error' },
      match: { params: '' },
      OAuthSuccess: jest.fn(),
      OAuthFailed: jest.fn()
    };
    const callback = shallow(<Callback {...props} />);
    expect(props.OAuthFailed).toHaveBeenCalledWith('fake-error');
    expect(callback.find(Redirect)).toHaveLength(1);
  });

  it('should call oauth success if no error, provider and access_token and return redirect', () => {
    const props = {
      location: { hash: 'access_token=fake-token', search: '' },
      match: { params: { provider: 'fake-provider' } },
      OAuthSuccess: jest.fn(),
      OAuthFailed: jest.fn()
    };
    const callback = shallow(<Callback {...props} />);
    expect(props.OAuthSuccess).toHaveBeenCalledWith('fake-token', 'fake-provider');
    expect(callback.find(Redirect)).toHaveLength(1);
  });

  it('should call nothing if no error but no provider and return redirect', () => {
    const props = {
      location: { hash: '', search: '' },
      match: { params: { provider: 'fake-provider' } },
      OAuthSuccess: jest.fn(),
      OAuthFailed: jest.fn()
    };
    const callback = shallow(<Callback {...props} />);
    expect(props.OAuthFailed).not.toHaveBeenCalled();
    expect(props.OAuthSuccess).not.toHaveBeenCalled();
    expect(callback.find(Redirect)).toHaveLength(1);
  });

  it('should call nothing if no error but no access token and return redirect', () => {
    const props = {
      location: { hash: 'access_token=fake-token', search: '' },
      match: { params: {} },
      OAuthSuccess: jest.fn(),
      OAuthFailed: jest.fn()
    };
    const callback = shallow(<Callback {...props} />);
    expect(props.OAuthFailed).not.toHaveBeenCalled();
    expect(props.OAuthSuccess).not.toHaveBeenCalled();
    expect(callback.find(Redirect)).toHaveLength(1);
  });
});
