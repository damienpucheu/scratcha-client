import React from 'react';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import App from '../../components/App';

describe('App component', () => {
  const initialState = { output: 100 };
  const mockStore = configureStore();

  it('renders without crashing', () => {
    const store = mockStore(initialState);
    const appComponent = shallow(<App store={store} />);
    expect(appComponent.length).toEqual(1);
  });

  it('should render correctly with no props', () => {
    const component = shallow(<App />);
    expect(component).toMatchSnapshot();
  });
});
