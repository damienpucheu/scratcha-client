import React from 'react';
import { shallow } from 'enzyme';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { providers } from '../../constants';
import LoginBox from '../../components/LoginBox';

describe('LoginBox', () => {
  const props = {
    callProvider: jest.fn(),
    loading: false
  };
  it('renders without crashing', () => {
    const loginBox = shallow(<LoginBox {...props} />);
    expect(loginBox.length).toEqual(1);
  });

  it('should render correctly with props', () => {
    const loginBox = shallow(<LoginBox {...props} />);
    expect(loginBox).toMatchSnapshot();
  });

  describe('loading false', () => {
    it('should render connect button', () => {
      const loginBox = shallow(<LoginBox {...props} />);
      expect(loginBox.exists('button')).toBe(true);
    });

    it('should call provider when click on button', () => {
      const loginBox = shallow(<LoginBox {...props} />);
      loginBox
        .find('button')
        .first()
        .simulate('click');
      expect(props.callProvider).toHaveBeenCalledWith(providers.instagram);
    });
  });

  describe('loading true', () => {
    it('should render loading icon', () => {
      const loginBox = shallow(<LoginBox loading callProvider={props.callProvider} />);
      expect(loginBox.exists('button')).toBe(false);
      expect(loginBox.find(FontAwesomeIcon)).toHaveLength(1);
    });
  });
});
