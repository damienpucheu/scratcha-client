import React from 'react';
import { shallow } from 'enzyme';
import Avatar from '../../components/Avatar';

describe('Avatar', () => {
  const props = {};
  it('renders without crashing', () => {
    const avatar = shallow(<Avatar {...props} />);
    expect(avatar.length).toEqual(1);
  });

  it('should render correctly with empty props', () => {
    const avatar = shallow(<Avatar {...props} />);
    expect(avatar).toMatchSnapshot();
  });
});
