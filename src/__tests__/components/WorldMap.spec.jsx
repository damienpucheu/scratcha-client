import React from 'react';
import { shallow } from 'enzyme';
import WorldMap from '../../components/WorldMap';

describe('worldmap', () => {
  const props = {};
  it('renders without crashing', () => {
    const worldmap = shallow(<WorldMap {...props} />);
    expect(worldmap.length).toEqual(1);
  });

  it('should render correctly with empty props', () => {
    const worldmap = shallow(<WorldMap {...props} />);
    expect(worldmap).toMatchSnapshot();
  });
});
