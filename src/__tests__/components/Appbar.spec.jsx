import React from 'react';
import { shallow } from 'enzyme';
import Avatar from '../../components/Avatar';
import Appbar from '../../components/Appbar';

describe('Appbar', () => {
  const props = {};
  it('renders without crashing', () => {
    const appbar = shallow(<Appbar {...props} />);
    expect(appbar.length).toEqual(1);
  });

  it('should render correctly with empty props', () => {
    const appbar = shallow(<Appbar {...props} />);
    expect(appbar).toMatchSnapshot();
  });

  describe('no username in props', () => {
    it('should no render avatar', () => {
      const appbar = shallow(<Appbar {...props} />);
      expect(appbar.find(Avatar)).toHaveLength(0);
    });
  });

  describe('username in props', () => {
    it('should render avatar', () => {
      const appbar = shallow(<Appbar username="fake-user" />);
      expect(appbar.find(Avatar)).toHaveLength(1);
    });
  });
});
