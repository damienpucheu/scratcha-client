import * as actions from '../../actions/userActions';
import * as types from '../../constants';

describe('actions', () => {
  it('should create an action to fetch user', () => {
    const expectedAction = {
      type: types.FETCH_USER,
    };
    expect(actions.fetchUser()).toEqual(expectedAction);
  });
  it('should create an action when fetch user failed', () => {
    const error = 'fake-error';
    const expectedAction = {
      type: types.FETCH_USER_FAILED,
      error,
    };
    expect(actions.fetchUserFailed(error)).toEqual(expectedAction);
  });
  it('should create an action when fetch user succeeded', () => {
    const user = 'fake-user';
    const expectedAction = {
      type: types.FETCH_USER_SUCCESS,
      user,
    };
    expect(actions.fetchUserSuccess(user)).toEqual(expectedAction);
  });
  it('should create an action when save token in store', () => {
    const accessToken = 'fake-token';
    const expectedAction = {
      type: types.SAVE_TOKEN_IN_STORE,
      accessToken,
    };
    expect(actions.saveTokenInStore(accessToken)).toEqual(expectedAction);
  });
});
