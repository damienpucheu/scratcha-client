import * as actions from '../../actions/mapActions';
import * as types from '../../constants';

describe('actions', () => {
  it('should create an action to select country', () => {
    const country = 'fake-country';
    const expectedAction = {
      type: types.SELECT_COUNTRY,
      country
    };
    expect(actions.selectCountry(country)).toEqual(expectedAction);
  });
});
