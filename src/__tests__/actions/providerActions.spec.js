import * as actions from '../../actions/providerActions';
import * as types from '../../constants';

describe('actions', () => {
  it('should create an action to call oauth provider', () => {
    const provider = 'fake-provider';
    const expectedAction = {
      type: types.CALL_OAUTH_PROVIDER,
      provider,
    };
    expect(actions.callOAuthProvider(provider)).toEqual(expectedAction);
  });
  it('should create an action to call oauth provider failed', () => {
    const error = 'fake-error';
    const expectedAction = {
      type: types.CALL_OAUTH_PROVIDER_FAILED,
      error,
    };
    expect(actions.callOAuthProviderFailed(error)).toEqual(expectedAction);
  });
  it('should create an action to call oauth provider success', () => {
    const accessToken = 'fake-token';
    const expectedAction = {
      type: types.CALL_OAUTH_PROVIDER_SUCCESS,
      accessToken,
    };
    expect(actions.callOAuthProviderSuccess(accessToken)).toEqual(expectedAction);
  });
});
