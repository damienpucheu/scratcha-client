import React from 'react';
import { shallow } from 'enzyme';
import HomePage from '../../screens/HomePage';

describe('HomePage component', () => {
  const props = {
    callProvider: jest.fn(),
    user: { loading: false }
  };
  it('renders without crashing', () => {
    const homeComponent = shallow(<HomePage {...props} />);
    expect(homeComponent.length).toEqual(1);
  });

  it('should render correctly with props', () => {
    const homeComponent = shallow(<HomePage {...props} />);
    expect(homeComponent).toMatchSnapshot();
  });
});
