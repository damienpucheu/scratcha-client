// USER TYPES
export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILED = 'FETCH_USER_FAILED';
export const SAVE_TOKEN_IN_STORE = 'SAVE_TOKEN_IN_STORE';

// MAP TYPES
export const SELECT_COUNTRY = 'SELECT_COUNTRY';

// PROVIDER TYPES
export const CALL_OAUTH_PROVIDER = 'CALL_OAUTH_PROVIDER';
export const CALL_OAUTH_PROVIDER_FAILED = 'CALL_OAUTH_PROVIDER_FAILED';
export const CALL_OAUTH_PROVIDER_SUCCESS = 'CALL_OAUTH_PROVIDER_SUCCESS';

// PROVIDERS LIST
export const providers = {
  instagram: {
    name: 'INSTAGRAM',
    authEndpoint: `https://api.instagram.com/oauth/authorize/?client_id=${process.env.REACT_APP_IG_CLIENT_ID}&redirect_uri=${process.env.REACT_APP_IG_REDIRECT_URI}&response_type=token`,
    profileEndpoint: 'https://api.instagram.com/v1/users/self/'
  }
};

// HTTP METHODS
export const GET = 'GET';
export const POST = 'POST';
