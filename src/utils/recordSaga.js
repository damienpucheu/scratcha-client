import { runSaga } from 'redux-saga';

const recordSaga = async (saga, initialAction) => {
  const dispatched = [];
  await runSaga(
    {
      dispatch: action => dispatched.push(action),
      getState: () => {},
      call: promise => promise
    },
    saga,
    initialAction
  ).done;

  return dispatched;
};

export default recordSaga;
