/**
 * call url with http method
 * @param {*} url
 * @param {*} method
 */

const callApi = async (url, method) => {
  const options = {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };
  try {
    const response = await fetch(url, options);
    const data = await response.json();
    return data;
  } catch (e) {
    throw e;
  }
};

export default callApi;
