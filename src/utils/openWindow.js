const openOAuthLoginPage = url => window.location.replace(url);

export default openOAuthLoginPage;
