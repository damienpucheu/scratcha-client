import {
  CALL_OAUTH_PROVIDER,
  CALL_OAUTH_PROVIDER_FAILED,
  CALL_OAUTH_PROVIDER_SUCCESS,
  providers
} from '../constants';

export const callOAuthProvider = provider => ({
  type: CALL_OAUTH_PROVIDER,
  provider
});

export const callOAuthProviderFailed = error => ({
  type: CALL_OAUTH_PROVIDER_FAILED,
  error
});

export const callOAuthProviderSuccess = (accessToken, provider) => ({
  type: CALL_OAUTH_PROVIDER_SUCCESS,
  accessToken,
  provider: providers[provider]
});
