import { SELECT_COUNTRY } from '../constants';

export const selectCountry = country => ({
  type: SELECT_COUNTRY,
  country
});

export const unselectCountry = {};
