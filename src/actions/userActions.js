import {
  FETCH_USER,
  FETCH_USER_FAILED,
  FETCH_USER_SUCCESS,
  SAVE_TOKEN_IN_STORE
} from '../constants';

export const fetchUser = () => ({
  type: FETCH_USER
});

export const fetchUserFailed = error => ({
  type: FETCH_USER_FAILED,
  error
});

export const fetchUserSuccess = user => ({
  type: FETCH_USER_SUCCESS,
  user
});

export const saveTokenInStore = accessToken => ({
  type: SAVE_TOKEN_IN_STORE,
  accessToken
});
