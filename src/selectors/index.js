export const getUser = state => state.user;
export const getToken = state => state.user.accessToken;
export const getProvider = state => state.provider;
