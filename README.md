# Scratcha

[![pipeline status](https://gitlab.com/damienpucheu/scratcha-client/badges/master/pipeline.svg)](https://gitlab.com/damienpucheu/scratcha-client/commits/master)
[![coverage report](https://gitlab.com/damienpucheu/scratcha-client/badges/master/coverage.svg)](https://gitlab.com/damienpucheu/scratcha-client/commits/master)
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

## Goal 
Scratcha aims to scratch a world map thanks to your instagram pictures and the countries that you have visited.